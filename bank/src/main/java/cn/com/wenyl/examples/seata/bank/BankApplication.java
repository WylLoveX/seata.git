package cn.com.wenyl.examples.seata.bank;

import cn.com.wenyl.examples.seata.bank.config.DataSourceProxyConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;

/**
 * @author Mr.Wen
 * @version 1.0
 * @date 2021-10-21 16:59
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableDiscoveryClient
@Import(DataSourceProxyConfig.class)
public class BankApplication {
    public static void main(String[] args){
        SpringApplication.run(BankApplication.class);
    }
}
