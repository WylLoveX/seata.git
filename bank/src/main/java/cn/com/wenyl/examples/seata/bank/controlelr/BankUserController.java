package cn.com.wenyl.examples.seata.bank.controlelr;

import cn.com.wenyl.examples.seata.bank.service.BankUserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author Mr.Wen
 * @version 1.0
 * @date 2021-10-25 14:06
 */
@RestController
@RequestMapping(value = "/bank")
public class BankUserController {
    @Resource
    private BankUserService bankUserService;

    @ResponseBody
    @GetMapping("/reduceAccount")
    public int reduceAccount(@RequestParam("id")String id,@RequestParam("num")Integer num){
        return bankUserService.reduceAccount(id,num);
    }
}
