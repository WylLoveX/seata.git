package cn.com.wenyl.examples.seata.bank.dao;

import cn.com.wenyl.examples.seata.bank.entity.BankUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Mapper
public interface BankUserMapper {
    int deleteByPrimaryKey(String id);

    int insert(BankUser record);

    int insertSelective(BankUser record);

    BankUser selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BankUser record);

    int updateByPrimaryKey(BankUser record);

    int reduceAccount(@Param("id") String id, @Param("num") Integer num);
}