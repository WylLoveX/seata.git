package cn.com.wenyl.examples.seata.bank.entity;

public class BankUser {
    private String id;

    private String userName;

    private Long userAmount;

    public BankUser(String id, String userName, Long userAmount) {
        this.id = id;
        this.userName = userName;
        this.userAmount = userAmount;
    }

    public BankUser() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public Long getUserAmount() {
        return userAmount;
    }

    public void setUserAmount(Long userAmount) {
        this.userAmount = userAmount;
    }
}