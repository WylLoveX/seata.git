package cn.com.wenyl.examples.seata.bank.service;

import cn.com.wenyl.examples.seata.bank.entity.BankUser;

/**
 * @author Mr.Wen
 * @version 1.0
 * @date 2021-10-25 14:03
 */
public interface BankUserService {
    int reduceAccount(String id,Integer num);
}
