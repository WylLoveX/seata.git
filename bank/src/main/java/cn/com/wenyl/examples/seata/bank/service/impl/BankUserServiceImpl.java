package cn.com.wenyl.examples.seata.bank.service.impl;

import cn.com.wenyl.examples.seata.bank.dao.BankUserMapper;
import cn.com.wenyl.examples.seata.bank.entity.BankUser;
import cn.com.wenyl.examples.seata.bank.service.BankUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Mr.Wen
 * @version 1.0
 * @date 2021-10-25 14:04
 */
@Service
public class BankUserServiceImpl implements BankUserService {
    @Resource
    private BankUserMapper bankUserMapper;

    @Override
    @Transactional
    public int reduceAccount(String id,Integer num) {
        int i=1/0;
        return bankUserMapper.reduceAccount(id,num);
    }
}
