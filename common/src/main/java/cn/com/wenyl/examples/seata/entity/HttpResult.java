package cn.com.wenyl.examples.seata.entity;

import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 *
 * @author Mr.Wen
 * 这是一个封装HTTP返回值信息的类，不做赘述
 */
public class HttpResult implements Serializable {
    private Integer code;
    private String msg;
    private Object data;
    public HttpResult() {
    }
    private HttpResult(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }


    public static HttpResult success(){
        return new HttpResult(HttpStatus.OK.value(), "成功" , null);
    }

    public static HttpResult success(String message){
        return new HttpResult(HttpStatus.OK.value(), message , null);
    }

    public static HttpResult success(Object data){
        return new HttpResult(HttpStatus.OK.value(), "成功" , data);
    }
    public static HttpResult success(String message,Object data){
        return new HttpResult(HttpStatus.OK.value(), message , data);
    }

    public static HttpResult error(){
        return new HttpResult(HttpStatus.INTERNAL_SERVER_ERROR.value(), "失败" , null);
    }

    public static HttpResult error(String message){
        return new HttpResult(HttpStatus.INTERNAL_SERVER_ERROR.value(), message , null);
    }

    public static HttpResult error(Object data){
        return new HttpResult(HttpStatus.INTERNAL_SERVER_ERROR.value(), "失败" , data);
    }
    public static HttpResult error(String message,Object data){
        return new HttpResult(HttpStatus.INTERNAL_SERVER_ERROR.value(), message , data);
    }

    public static HttpResult customCode(int code,String message,Object data){
        return new HttpResult(code, message , data);
    }

    public static HttpResult unauthorized(){
        return new HttpResult(HttpStatus.UNAUTHORIZED.value(), "请先认证", null);
    }

}

