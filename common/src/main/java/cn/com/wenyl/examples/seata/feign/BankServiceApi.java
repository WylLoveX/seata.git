package cn.com.wenyl.examples.seata.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Mr.Wen
 * @version 1.0
 * @date 2021-10-25 11:49
 */
@Component
@FeignClient(name = "bank")
public interface BankServiceApi {
    /**
     * 减少银行卡金额
     * @param id 用户id
     * @param num 金额
     * @return 返回操作结果
     */
    @GetMapping("/bank/reduceAccount")
    int reduceAccount(@RequestParam String id, @RequestParam Integer num);
}
