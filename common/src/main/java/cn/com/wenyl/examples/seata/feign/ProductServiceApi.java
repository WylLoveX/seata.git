package cn.com.wenyl.examples.seata.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Mr.Wen
 * @version 1.0
 * @date 2021-10-25 11:49
 */
@Component
@FeignClient(value = "product")
public interface ProductServiceApi {
    /**
     * 减少产品库存
     * @param id 产品id
     * @param num 数量
     * @return 返回操作结果
     */
    @GetMapping("/product/reduceStock")
    int reduceStock(@RequestParam String id, @RequestParam Integer num);
}
