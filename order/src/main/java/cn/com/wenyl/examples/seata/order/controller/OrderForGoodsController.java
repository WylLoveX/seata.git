package cn.com.wenyl.examples.seata.order.controller;

import cn.com.wenyl.examples.seata.order.entity.OrderForGoods;
import cn.com.wenyl.examples.seata.order.service.OrderForGoodsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author Mr.Wen
 * @version 1.0
 * @date 2021-10-25 15:52
 */
@RestController
@RequestMapping("/order")
public class OrderForGoodsController {
    @Resource
    private OrderForGoodsService orderForGoodsService;

    @GetMapping("/insert")
    @ResponseBody
    public int insert(OrderForGoods orderForGoods){
        return orderForGoodsService.insert(orderForGoods);
    }
    @GetMapping("/hello")
    public int insert(){
        return orderForGoodsService.hello();
    }
}
