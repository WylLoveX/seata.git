package cn.com.wenyl.examples.seata.order.dao;

import cn.com.wenyl.examples.seata.order.entity.OrderForGoods;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderForGoodsMapper {
    int deleteByPrimaryKey(String id);

    int insert(OrderForGoods record);

    int insertSelective(OrderForGoods record);

    OrderForGoods selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(OrderForGoods record);

    int updateByPrimaryKey(OrderForGoods record);
}