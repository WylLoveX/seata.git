package cn.com.wenyl.examples.seata.order.entity;

public class OrderForGoods {
    private String id;

    private String productId;

    private String userId;

    private Integer productNum;

    public OrderForGoods(String id, String productId, String userId, Integer productNum) {
        this.id = id;
        this.productId = productId;
        this.userId = userId;
        this.productNum = productNum;
    }

    public OrderForGoods() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId == null ? null : productId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public Integer getProductNum() {
        return productNum;
    }

    public void setProductNum(Integer productNum) {
        this.productNum = productNum;
    }
}