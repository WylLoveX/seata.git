package cn.com.wenyl.examples.seata.order.service;

import cn.com.wenyl.examples.seata.order.entity.OrderForGoods;

/**
 * @author Mr.Wen
 * @version 1.0
 * @date 2021-10-25 15:49
 */
public interface OrderForGoodsService {
    int insert(OrderForGoods orderForGoods);

    int hello();
}
