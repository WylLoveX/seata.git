package cn.com.wenyl.examples.seata.order.service.impl;

import cn.com.wenyl.examples.seata.order.dao.OrderForGoodsMapper;
import cn.com.wenyl.examples.seata.order.entity.OrderForGoods;
import cn.com.wenyl.examples.seata.order.service.OrderForGoodsService;
import cn.com.wenyl.examples.seata.feign.BankServiceApi;
import cn.com.wenyl.examples.seata.feign.ProductServiceApi;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author Mr.Wen
 * @version 1.0
 * @date 2021-10-25 15:49
 */
@Service
public class OrderForGoodsServiceImpl implements OrderForGoodsService {
    @Resource
    private OrderForGoodsMapper orderForGoodsMapper;
    @Resource
    private ProductServiceApi productServiceApi;
    @Resource
    private BankServiceApi bankServiceApi;

    @Override
    @GlobalTransactional
    public int insert(OrderForGoods orderForGoods){
        int ret = orderForGoodsMapper.insert(orderForGoods);
        productServiceApi.reduceStock("1",1);
        bankServiceApi.reduceAccount("1",10);
        return ret;
    }

    @Override
    @GlobalTransactional(name = "hello-order",rollbackFor = Exception.class)
    public int hello() {
        OrderForGoods orderForGoods = new OrderForGoods();
        orderForGoods.setId("1");
        orderForGoods.setProductId("1");
        orderForGoods.setUserId("1");
        orderForGoods.setProductNum(1);
        int ret = orderForGoodsMapper.insert(orderForGoods);
        productServiceApi.reduceStock("1",1);
        bankServiceApi.reduceAccount("1",10);
        return ret;
    }
}
