package cn.com.wenyl.examples.seata.product.controller;

import cn.com.wenyl.examples.seata.product.service.ProductService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author Mr.Wen
 * @version 1.0
 * @date 2021-10-25 15:33
 */
@RestController
@RequestMapping("product")
public class ProductController {
    @Resource
    private ProductService productService;

    @GetMapping("/reduceStock")
    public int reduceStock(@RequestParam String id, @RequestParam Integer num) {
        return productService.reduceStock(id,num);
    }
}
