package cn.com.wenyl.examples.seata.product.entity;

public class Product {
    private String id;

    private String productName;

    private Long productStock;

    public Product(String id, String productName, Long productStock) {
        this.id = id;
        this.productName = productName;
        this.productStock = productStock;
    }

    public Product() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName == null ? null : productName.trim();
    }

    public Long getProductStock() {
        return productStock;
    }

    public void setProductStock(Long productStock) {
        this.productStock = productStock;
    }
}