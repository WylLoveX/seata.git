package cn.com.wenyl.examples.seata.product.service;

/**
 * @author Mr.Wen
 * @version 1.0
 * @date 2021-10-25 15:23
 */
public interface ProductService {
    int reduceStock(String id,Integer num);
}
