package cn.com.wenyl.examples.seata.product.service.impl;

import cn.com.wenyl.examples.seata.product.dao.ProductMapper;
import cn.com.wenyl.examples.seata.product.service.ProductService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author Mr.Wen
 * @version 1.0
 * @date 2021-10-25 15:24
 */
@Service
public class ProductServiceImpl implements ProductService {
    @Resource
    private ProductMapper productMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int reduceStock(String id, Integer num) {
        return productMapper.reduceStock(id,num);
    }
}
